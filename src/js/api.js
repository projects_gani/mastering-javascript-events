async function postData(url = "", data = {}) {
    const response = await fetch(url, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json"
        },
        redirect: "follow",
        referrerPolicy: "no-referrer",
        body: JSON.stringify(data)
    });
    return response.json();
}

async function getData(url = "") {
    const response = await fetch(url, {
        method: "GET",
    });
    return response.json();
}

export async function subscribe(email) {
    const data = await postData("http://localhost:3000/subscribe", { email });
    return data;
}

export async function unsubscribe() {
    const data = await postData("http://localhost:3000/unsubscribe");
    return data;
}

export async function getCommunity() {
    const data = await getData("http://localhost:3000/community");
    return data;
}

export async function getUserAnalytics(buttonData) {
    const data = await postData("http://localhost:3000/analytics/user", { buttonData });
    return data;
}

export async function getPerformanceAnalytics(metrics) {
    const data = await postData("http://localhost:3000/analytics/performance", metrics);
    return data;
}


