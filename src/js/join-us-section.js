/* eslint-disable no-mixed-spaces-and-tabs */
import { validate } from "./email-validator.js";
import { subscribe } from "./api.js";
import { unsubscribe } from "./api.js";

class JoinOurProgram {
	constructor(title, btnTitle) {
		this.title = title;
		this.btnTitle = btnTitle;
	}

	sectionRender() {
		const sectionOurProgram = document.createElement("section");
		const sectionTitle = document.createElement("h2");
		const sectionSubTitle = document.createElement("h3");
		const form = document.createElement("form");
		const input = document.createElement("input");
		const button = document.createElement("button");
		const unSubBtn = document.createElement("button");

		const isSubscribed = () => localStorage.getItem("subscribed") === "true";

		const renderForm = () => {
			if (isSubscribed()) {
				input.style.display = "none";
				button.style.display = "none";
				unSubBtn.style.display = "block";
			} else {
				input.value = "";
				input.style.display = "block";
				button.style.display = "block";
				unSubBtn.style.display = "none";
			}
		};

		const disableButton = (typeBtn = button) => {
			typeBtn.style.opacity = "0.5";
			typeBtn.disabled = true;
		}

		const enableButton = () => {
			button.style.opacity = "1";
			button.disabled = false;
		}
		
		sectionOurProgram.classList.add("app-section", "app-section--image-program");

		sectionTitle.classList.add("app-title");
		sectionTitle.textContent = this.title;
		sectionOurProgram.appendChild(sectionTitle);

		sectionSubTitle.classList.add("app-subtitle");
		sectionSubTitle.innerHTML = "Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.";
		sectionOurProgram.appendChild(sectionSubTitle);

		form.classList.add("data");
		sectionOurProgram.appendChild(form);
		
		input.type = "email";
		input.classList.add("app-section__email");
		input.placeholder = "Email";

		input.value = localStorage.getItem("email");

		form.appendChild(input);

		button.type = "submit";
		button.classList.add("app-section__button", "app-section__button--subscribe");
		button.innerHTML = this.btnTitle;
		form.appendChild(button);

		
		unSubBtn.type = "submit";
		unSubBtn.classList.add("app-section__button", "app-section__button--unsubscribe");
		unSubBtn.innerHTML = "Unsubscribe";
		unSubBtn.style.display = "none";
		form.appendChild(unSubBtn);

		input.addEventListener("input", function() {
			localStorage.setItem("email", input.value);
		});

		form.addEventListener("submit", function(e) {
			e.preventDefault();
		});

		button.addEventListener("click", async function(event) {
			disableButton();
			const isValid = validate(input.value);
			if (isValid) {
				const response = await subscribe(localStorage.getItem("email"));
				if (response.success) {
					localStorage.setItem("subscribed", true);
					renderForm();
				} else if (response.error) {
					alert(response.error);
				}
			} else {
				alert("Your email should end with 'gmail.com' or 'outlook.com'");
			}
			enableButton();
		});

		unSubBtn.addEventListener("click", async function() {
			disableButton(unSubBtn);
			const response = await unsubscribe();
			if (response.success) {
				localStorage.setItem("subscribed", false);
				localStorage.removeItem("email");
				renderForm();
			}
			enableButton();
		});

		return sectionOurProgram;
	}
}

export class JoinOurProgramFactory {
	createJoinOurSection(type) {
		if (type === "advanced") {
			return new JoinOurProgram("Join Our Advanced Program", "Subscribe Advanced").sectionRender();
		} 
		else if (type === "standard") { 
			return new JoinOurProgram("Join Our Program", "Subscribe").sectionRender();
		}
	}
}
