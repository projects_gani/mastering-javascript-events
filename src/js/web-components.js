class WebsiteSection extends HTMLElement {
	constructor() {
	  super();
	  const template = document.createElement('template');
	  template.innerHTML = `
		<section class="app-section">
		  <slot></slot>
		  <h2>
		 	<slot name="title"></slot> 
		  </h2>
		  <button><slot name="button-play"></slot></button>
		  <h3>
		  	<slot name="description"></slot>
		  </h3>
		  <article>
		  	<slot name="content"></slot>
		  </article>
		  <button><slot name="button-more"></slot></button>
		</section>
	  `;
	  const shadowRoot = this.attachShadow({ mode: 'open' });
	  shadowRoot.innerHTML = `
	  <style>
		button {
			border:none;
			background-color: unset;
		}
		.app-section {
			display: flex;
			align-items: center;
			justify-content: center;
			flex-direction: column;
		}
	  </style>`
	  this.shadowRoot.appendChild(template.content.cloneNode(true));
	}
  }
  
  window.customElements.define('website-section', WebsiteSection);