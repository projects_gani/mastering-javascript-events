import { getCommunity, getPerformanceAnalytics } from "./api";

export class Performance {
    measureFetchPerformance() {
        const communityFetchStart = performance.now();
        return getCommunity()
            .then((response) => {
                const communityFetchEnd = performance.now();
                const communityFetchTime = communityFetchEnd - communityFetchStart;
                console.log("Community fetch info time: " + communityFetchTime);
                console.log(response);
                const metrics = {
                    fetchTime: communityFetchTime,
                    pageLoad: this.measurePageLoad(),
                    memoryUsage: this.measureMemoryUsage()
                }

                getPerformanceAnalytics(metrics)
                    .then((response) => {
                        console.log("Metrics that has been send to server " + response);
                    }); 
                
                return response;
            });
    }

    measurePageLoad() {
        const pageTiming = performance.getEntriesByType("navigation");
        const navigationTiming = pageTiming[0];
        const pageFullyReady = navigationTiming.loadEventEnd - navigationTiming.fetchStart;

        console.log("Page loaded " + pageFullyReady);
        return pageFullyReady;
    }

    measureMemoryUsage() {
        const usedMemory = performance.memory.usedJSHeapSize; 
        console.log("Memory used "  + usedMemory + " bytes");
        return usedMemory;
    }

}