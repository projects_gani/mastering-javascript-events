
export function communityRender(data) {
		
    const sectionCommunity = document.createElement("section");
    const sectionTitle = document.createElement("h2");
    const sectionSubTitle = document.createElement("h3");
    const feedbackDiv = document.createElement("div");


    sectionCommunity.classList.add("app-section", "app-section--feedback");
    feedbackDiv.classList.add("app-feedback");
    sectionTitle.classList.add("app-title");
    sectionSubTitle.classList.add("app-subtitle");

    sectionTitle.innerHTML = "Big Community of <br> People Like You";
    sectionCommunity.appendChild(sectionTitle);
    
    sectionSubTitle.innerHTML = "We're proud of our products, and we're really excited <br> when we get feedback from our users.";
    sectionCommunity.appendChild(sectionSubTitle);

    const html = data.map(function(item) {
        const feedbackWrap = document.createElement("div");
        feedbackWrap.classList.add("app-feedback__item");

        const userAvatar = document.createElement("img");
        userAvatar.classList.add("app-feedback__item--img")
        userAvatar.src = item.avatar;
        userAvatar.alt = "";

        const userFeedback = document.createElement("p");
        userFeedback.innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.";
        
        const userName = document.createElement("h4");
        userName.innerHTML = `${item.firstName} ${item.lastName}`;
        userName.classList.add("app-feedback__userName");

        const userPosition = document.createElement("p");
        userPosition.innerHTML = `${item.position}`;

        
        feedbackWrap.appendChild(userAvatar);
        feedbackWrap.appendChild(userFeedback);
        feedbackWrap.appendChild(userName);
        feedbackWrap.appendChild(userPosition);

        feedbackDiv.appendChild(feedbackWrap);

        return feedbackDiv;
    }
        
    );

    sectionCommunity.appendChild(feedbackDiv);
            
    return sectionCommunity;
}