const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com"];

export function validate(emailString) {
	if (typeof emailString === "string" && emailString !== "") {
		for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
			if (emailString.endsWith(VALID_EMAIL_ENDINGS[i])) {
				return true;
			}
		}
	} 
	return false;
	
}

export function validateAsync(emailString) {
	return new Promise((resolve) => {
		if (typeof emailString === "string") {
			for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
				if (emailString.endsWith(VALID_EMAIL_ENDINGS[i])) {
					resolve(true);
				}
			}
		}	
		resolve(false);
	});
}

export function validateWithThrow(emailString) {
	if (typeof emailString === "string") {
		for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
			if (emailString.endsWith(VALID_EMAIL_ENDINGS[i])) {
				return true;
			}
		}
	} else return false;
	throw new Error("Invalid email: " + emailString); 
}

export function validateWithLog(emailString) {
	if (typeof emailString === "string") {
		for (let i = 0; i < VALID_EMAIL_ENDINGS.length; i++) {
			if (emailString.endsWith(VALID_EMAIL_ENDINGS[i])) {
				console.log(`Email ${emailString} is valid.`);
				return true;
			}
		}
	} else console.log("not string");
	console.log(`Email ${emailString} is invalid.`);
	return false;
}