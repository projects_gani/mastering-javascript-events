/* eslint-disable no-mixed-spaces-and-tabs */
import { JoinOurProgramFactory } from "./join-us-section.js";
import { communityRender } from "./community.js";
import "../styles/style.css";
import Worker from "./clicks.worker.js";
import { Performance } from "./performance.js";

const performance = new Performance();
const createSection = new JoinOurProgramFactory();


document.addEventListener("DOMContentLoaded", async () => {
	const readySection = createSection.createJoinOurSection("standard");
	const footer = document.querySelector(".app-footer");
	footer.before(readySection);

	const community = await performance.measureFetchPerformance();
	const communitySection = communityRender(community);

	const learnSection = document.querySelector(".app-section--image-culture");
	learnSection.before(communitySection);

	const worker = new Worker("./clicks.worker.js");
	const buttons = document.getElementsByTagName("button");
	const emailInput = document.querySelector(".app-section__email");

	for (let i = 0; i < buttons.length; i++) {
		buttons[i].addEventListener("click", function(event) {
			const clickData = {
				type: "click",
				target: event.target.tagName
			}
		
			worker.postMessage(clickData);
		});
	}

	emailInput.addEventListener("focus", function(event) {
		const clickData = {
			type: "click",
			target: event.target.tagName
		}
	
		worker.postMessage(clickData);
	})
	
});