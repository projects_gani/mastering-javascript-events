import { getUserAnalytics } from "./api";

const BATCH_SIZE = 5;
let batch = [];

self.onmessage = function(event) {
  batch.push(event.data);

  if (batch.length === BATCH_SIZE) {
    getUserAnalytics(batch);
    batch.length = 0;
  }
};