import { expect } from 'chai'; 
import { validate, validateAsync, validateWithThrow, validateWithLog } from '../js/email-validator.js';
let sinon = require("sinon");
 
describe("Email validation", () => { 
  it("should return false when passed empty value", () => {
    const expected = false;
    const actual = validate("");
    expect(actual).to.equal(expected);
  });
  
  it("should return false when incorrect ending email passed", () => {
    const expected = false;
    const actual = validate("ganios@gamail.com");
    expect(actual).to.equal(expected);
  });

  it("should return false when non-string value passed", () => {
    const expected = false;
    const actual = validate(123);
    expect(actual).to.equal(expected);
  });

  it("should return true when correct ending email passed", () => {
    const expected = true;
    const actual = validate("ganios@gmail.com");
    expect(actual).to.equal(expected);
  });
});

describe("Email validation in async function with promises", () => {
  it("should return false when passed empty value", () => {
    const expected = false;
    return validateAsync("")
      .then((res) => {
        expect(res).to.equal(expected);
      })
  });

  it("should return false when incorrect ending email passed", () => {
    const expected = false;
    return validateAsync("ganios@das.com")
      .then((res) => {
        expect(res).to.equal(expected);
      });
  });

  it("should return false when non-string value passed", () => {
    const expected = false;
    return validateAsync(123)
      .then((res) => {
        expect(res).to.equal(expected);
      });
  });

  it("should return true when correct ending email passed", () => {
    const expected = true;
    return validateAsync("ganios@gmail.com")
      .then((res) => {
        expect(res).to.equal(expected);
      });
  });
});

describe("Email validation with Error throw", () => {
  it("should throw error when email is invalid", () => {
    const email = "asd@asd.com";
    expect(() => {
      validateWithThrow(email);
    }).to.throw("Invalid email: " + email);
  });

  it("should return true when correct ending email passed", () => {
    const expected = true;
    const actual = validateWithThrow("ganios@gmail.com");
    expect(actual).to.equal(expected);
  });

  it("should return false when non-string value passed", () => {
    const expected = false;
    const actual = validateWithThrow(123);
    expect(actual).to.equal(expected);
  });
});

describe("validateWithLog", function() {
  let logStub;

  before(function() {
    logStub = sinon.stub(console, "log");
  });

  after(function() {
    logStub.restore();
  });

  it("should return true when correct ending email passed", function() {
    const actual = validateWithLog("ganios@gmail.com");
    expect(actual).to.be.true;
    expect(logStub.calledWith("Email ganios@gmail.com is valid.")).to.be.true;
  });

  it("should return false when incorrect ending email passed", function() {
    const actual = validateWithLog("ganios@gmail.as");
    expect(actual).to.be.false;
    expect(logStub.calledWith("Email ganios@gmail.as is invalid.")).to.be.true;
  });

  it("should return false when non-string passed", function() {
    const actual = validateWithLog(12);
    expect(actual).to.be.false;
    expect(logStub.calledWith("not string")).to.be.true;
  });
});