/* eslint-disable no-undef */
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: "./src/js/main.js",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js"
    },
    mode: "production",
    devtool: "source-map",
    devServer: {
        static: {
            directory: path.join(__dirname, "build")
        },
        compress: true,
        hot: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        }),
        new CopyPlugin({
            patterns: [
                { from: "src/assets/images/", to: "assets/images" },
            ]
        })
    ],
    module: {
        rules: [
            { test: /\.css$/, use: ["style-loader","css-loader"] },
            { test: /\.(js)$/, exclude: /[\\/]node_modules[\\/]/, use: { loader: "babel-loader" } },
            { test: /\.worker\.js$/, use: { loader: 'worker-loader' } }
        ]
    },
    performance : {
        hints : false
    }   
};