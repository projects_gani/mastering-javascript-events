/* eslint-disable no-undef */
module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": "eslint:recommended",
    "rules": {
    },
    "overrides": [
        {
            "files": ["./src/js/*.js"],
            "rules": {
              "semi": ["error", "always"],
              "quotes": ["error", "double"],
              "curly": ["error", "all"],
              "camelcase": ["error", {properties: "always"}],
              "eqeqeq": ["error", "always"],
              "prefer-const": ["error", {"destructuring": "any", "ignoreReadBeforeAssign": false}],
              "no-extra-parens": ["error", "all"],
              "no-multi-spaces": ["error", { ignoreEOLComments: true }],
              "no-unused-vars": ["warn", "always"],
            }
        }
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    }
};
